#!/usr/bin/env node

const childProcess = require('child_process');
const fs = require('fs');
const util = require('util');

const fsReadFile = (path) => util.promisify(fs.readFile)(path, { encoding: 'utf8' });
const fsWriteFile = (path, data) => util.promisify(fs.writeFile)(path, data, { encoding: 'utf8' });

function run(command, args) {
  return new Promise((resolve, reject) => {
    const spawnedProcess = childProcess.spawn(command, args, { stdio: 'inherit' });
    spawnedProcess.on('error', (err) => {
      reject(err);
    });
    spawnedProcess.on('close', (code) => {
      if (code === 0) {
        resolve();
      } else {
        reject(new Error(`Process exited with non-zero code ${code}`));
      }
    });
  });
}

function updateConnectKey(manifestContents, suffix) {
  const keyRegexp = /("?\bkey\b"?\s*:\s*"?)([a-zA-Z0-9-._]+)("?)/;
  const { lines } = manifestContents.split('\n').reduce(({ appEntryFound, lines, replaced }, currentLine) => {
    if (replaced) {
      return {
        lines: lines.concat([currentLine]),
        replaced,
        appEntryFound,
      };
    }
    if (appEntryFound && !replaced && keyRegexp.test(currentLine)) {
      const modifiedLine = currentLine.replace(keyRegexp, `$1$2${suffix}$3`);
      return {
        lines: lines.concat([modifiedLine]),
        replaced: true,
        appEntryFound: true,
      };
    }
    const appKeyLocation = /"?\bapp\b"?\s*:/.exec(currentLine);
    if (appKeyLocation && appKeyLocation.length) {
      const prefix = currentLine.substring(0, appKeyLocation.index);
      const fromMatch = currentLine.substring(appKeyLocation.index);
      if (keyRegexp.test(fromMatch)) {
        const modifiedFromMatch = fromMatch.replace(keyRegexp, `$1$2${suffix}$3`);
        return {
          lines: lines.concat([ prefix + modifiedFromMatch ]),
          appEntryFound: true,
          replaced: true,
        };
      } else {
        return {
          lines: lines.concat([currentLine]),
          appEntryFound: true,
          replaced: false,
        };
      }
    }
    return {
      lines: lines.concat([currentLine]), appEntryFound, replaced
    };
  }, { lines: [], replaced: false, appEntryFound: false });
  return lines.join('\n');
}

async function main() {
  const [, , environment = 'development'] = process.argv;
  const environmentSuffix = environment === 'production' ? '' : `.${environment}`;
  const originalManifest = await fsReadFile('manifest.yml');
  const manifestWithUpdatedConnectKey = updateConnectKey(originalManifest, environmentSuffix);
  try {
    await fsWriteFile('manifest.yml', manifestWithUpdatedConnectKey);
    await run('forge', ['deploy', '-e', environment]);
  } finally {
    await fsWriteFile('manifest.yml', originalManifest);
  }
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
