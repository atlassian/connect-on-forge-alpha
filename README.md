# Example app: Adopting Forge from Connect

This app provides an interactive example of a Confluence Connect app [adopting Forge](https://developer.atlassian.com/platform/adopting-forge-from-connect/how-to-adopt/)
(including remote capabilities).

This app's Forge adoption journey is split into four steps, tracked by four branches:

* `step/1-connect-app` - A Connect app...
* `step/2-register-on-forge` - ...that registers on Forge
* `step/3-adopt-forge-components` - ...and adopts two Forge modules...
* `step/4-consolidate-with-forge-remote` - ...and switches to a consolidated page

# Prerequisites

This tutorial is intended for maintainers of existing Connect apps built on
atlassian-connect-express, and as such assumes some familiarity with the
Connect app development process.

You will need:

* node.js version 18 or above (we use [nvm](https://github.com/nvm-sh/nvm) to manage our node versions).
* Forge CLI [set up and logged in](https://developer.atlassian.com/platform/forge/getting-started/#install-the-forge-cli).
* A Confluence site set up for testing, with development mode active, as per
  [these instructions](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#getting-started-with-connect).
* A tunnel set up to forward requests from the internet to port `3000`. If using ngrok, be sure to:
    * set up an authtoken (to avoid the 2-hour timeout) and click the confirmation on the warning page,
      as outlined [here](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-1--prepare-for-tunneling)
      and [here](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-1--prepare-for-tunneling).
    * keep the url stable by using the [free randomly-assigned static domain](https://dashboard.ngrok.com/cloud-edge/domains)
      or avoiding restarting ngrok. Changes to the url after the "register on forge" step will require
      an update to the manifest and a re-deployment.

# Step 1 - get the Connect app up and running

Start `ngrok` in a dedicated terminal, and keep it running for the duration.
Make a note of the domain for the next step.

```
ngrok http --domain=<your-assigned-static-subdomain>.ngrok-free.app 3000
```

In a separate terminal, merge in the step 1 branch and install the app's dependencies.

```
git merge step/1-connect-app
npm install
```

Then, edit `development.localBaseUrl` in `config.json` to point to your ngrok baseUrl.

Your Connect app is now ready to go!

```
npm start
```

Install it on your [development-mode-enabled](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-3--enable-development-mode-in-your-site)
Confluence Cloud site [the usual way](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-3--install-and-test-your-app).

### A quick tour of the Connect app's features

Our app, Bookmark It, allows users to bookmark pages of interest on Confluence so they can find them easily later.
It currently comprises two modules: a content byline item (to present a "Bookmark It" button) and a general page
located in the "Apps" dropdown (for browsing bookmarked pages). Bookmark a couple of your pages to try it out!

# Step 2 - register your app on Forge

We will now reigster our app on Forge, deploy it and install it onto our test site, as per steps 2-4 of
[How to adopt Forge from Connect](https://developer.atlassian.com/platform/adopting-forge-from-connect/how-to-adopt/#part-2--convert-your-descriptor-to-a-manifest).

If you have not yet [set up and logged in to the Forge CLI](https://developer.atlassian.com/platform/forge/getting-started/#install-the-forge-cli),
or have not upgraded your Forge CLI recently (`npm install -g @forge/cli@latest`), now's the time to do it.

Register, deploy and install by running the following four commands in your terminal.
The final command will prompt you for the address of your Confluence Cloud development site.

```bash
npx --yes connect-to-forge@latest --type confluence --url http://localhost:3000/atlassian-connect.json
forge register "Bookmark It"
forge deploy
forge install --confirm-scopes -p Confluence
```

At this point, your app will continue to function as before, but its "registered on Forge" status
is signified by the (Development) suffix that appears after extension point labels.
This feature is designed to aid in telling multiple environments of the same app apart
when they are installed together on a site. Production environments have no suffix appended.

You can now commit your changes and merge the step 2 branch. The only change on this branch is a
`manifest.sample.yml` file that you can compare with your own `manifest.yml`.

```bash
git add manifest.yml
git commit -am"Add app manifest"
git merge --no-edit step/2-register-on-forge
```
# Step 3 - add a new Forge-powered feature

Your Connect app is now on Forge, meaning it now has access to Forge modules.
Let's adopt one such Forge-only module, context menus, in our app.

Add the following to the end of your `manifest.yml`.
The first few lines must immediately follow the existing scopes, at the same level of indentation.

```yaml
    - read:content-details:confluence
    - read:content.property:confluence
    - write:content.property:confluence
    - read:app-system-token
    - storage:app
modules:
  confluence:contextMenu:
    - key: bookmark
      render: native
      resource: contextMenuBookmark
      title: Bookmark It
      resolver:
        function: resolver
  confluence:globalPage:
    - key: forge-bookmark-global-page
      title: Forge Bookmarks
      route: forge-bookmarks
      render: native
      resource: globalPage
      resolver:
        function: resolver
  function:
    - handler: resolver.handler
      key: resolver
resources:
  - key: contextMenuBookmark
    path: src/frontend/context.tsx
  - key: globalPage
    path: src/frontend/globalPage.tsx
```

Commit the changes and merge the step 3 branch. Once again, you can compare your manifest to the sample.

```bash
git commit -am "Adopt Forge modules"
git merge --no-edit step/3-adopt-forge-components
```

The corresponding Forge function code can be found in `src/index.tsx`

Let's reinstall, deploy and upgrade to our new version.
The final command will prompt you to select the installation to upgrade.

```bash
npm install
forge deploy
forge install --confirm-scopes --upgrade
```

### A quick tour of the new Forge features

Thanks to context menus, our app now supports bookmarking content within a page.
To try it out, select some text on a page, then in the context menu, under the drop-down
arrow, select "Bookmark it" and click "Bookmark it" to add the text to your bookmarks.

The text bookmarks can be viewed in the "Forge bookmarks" global page.

The bookmarked text is stored in Forge Storage, meaning the user-generated content
stays out of your database

# Step 4 - consolidate UI and data via Forge Remote

Both the Connect- and Forge-powered features and data are valuable, but it would be
nice to be able to consolidate these into one experience.

We can do so with the help of [Forge Remote Compute](https://developer.atlassian.com/platform/forge/forge-remote-overview/),
now in Preview and suported in an `atlassian-connect-express` prerelease
(final release and `atlassian-connect-spring-boot` support coming soon).

Forge Remote Compute is currently available for product events and from Custom UI, so we'll replace our
UI Kit global page and Connect General Page with a Custom UI global page.

This will require more changes to our manifest. You can either follow the steps below,
or simply `git merge step/4-consolidate-with-forge-remote` and copy all of the sample
manifest contents (except for your app ID and base URL) into your `manifest.yml`.

### Change 1: remove the Connect general page

Under `connectModules:`, delete the `confluence:generalPages:` entry.

### Change 2: remove the Forge global page and associated function

Under `modules:`, delete `confluence:globalPage:` entry.

Under `resources:` delete the lines `- key: globalPage` and `path: src/frontend/globalPage.tsx`

### Change 3: Add modules for Forge remote

Add the following under `modules` (at the same level of indentation as the other modules):

```yaml
  endpoint:
    - key: connect-endpoint
      remote: connect
      auth:
        appUserToken:
          enabled: false
        appSystemToken:
          enabled: true
  confluence:globalPage:
    - key: forge-remote-bookmark-global-page
      resource: main
      resolver:
        endpoint: connect-endpoint
      title: My Bookmarks
      route: forge-remote-bookmarks
```

### Change 4: Add the custom UI resource

At the end of `manifest.yml`, at the top level, add the following:

```yaml
resources:
  - key: main
    path: static/bookmarks/build
```

### Change 5: Add storage access permissions

### Configure your app ID

`atlassian-connect-express` needs to know your app ID in order to verify calls from Forge.
Set "appId" in the top level of your `config.json` to the full `app.id` value from your
`manifest.yml`

### Commit the changes

```bash
git commit -am "Consolidate via Forge remote"
```

### Merge, restart, redeploy, reinstall

If you haven't done so already, `git merge step/4-consolidate-with-forge-remote` and
check your `manifest.yml` against `manifest.sample.yml`.

In addition to the new Forge modules, this step adds a bundled, React-based front-end,
and new server logic, so we'll need to install new dependencies, build the front-end,
restart our server, and re-deploy our Forge app.

Bring down your app server, and then run the following commands.

```bash
npm install && npm run build
npm start
forge deploy
```

# What next?

From here, you might like to

* View the contents of your database using a GUI or CLI query tool to see how the Forge and Connect data are related
* View your app in the developer console to see what's available
