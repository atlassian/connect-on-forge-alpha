#!/usr/bin/env node

const crypto = require('crypto');
const fs = require('fs');
const http = require('http');
const https = require('https');
const path = require('path');
const queryString = require('querystring');

const handlers = {
  'GET': {},
  'PUT': {},
  'PATCH': {},
  'POST': {},
  'DELETE': {},
};

const appBaseUrl = process.env.BASE_URL;
const port = +(process.env.PORT || '49998');
if (!appBaseUrl) {
  console.error(`${new Date().toISOString()} 'BASE_URL' environment variable is required. Make sure it matches the baseUrl in your 'manifest.yml'`);
  process.exit(1);
}

/////////////////////////////////////////////
///////// INSTALLATION HANDLER //////////////
/////////////////////////////////////////////

async function saveInstallation(req, res) {
  const installationString = await getRequestBody(req);
  const installation = JSON.parse(installationString);

  const claims = await authenticateInstallHook(req);
  if (!claims || typeof claims === 'string') {
    const failureReason = claims || 'unknown error';
    console.log(`authenticating re-installation failed due to: ${failureReason}`);
    res.writeHead(403);
    res.end(failureReason);
    logRequest(req, res);
    return;
  }

  const destination = `data/installations/${installation.clientKey}.json`;
  await saveFile(destination, installationString);
  console.log('SAVED INSTALLATION:', installation);
  res.writeHead(204);
  res.end('');
  logRequest(req, res);
}

addHandler('POST', '/installed', saveInstallation);


/////////////////////////////////////////////
///////////////// WEBHOOK HANDLER ///////////
/////////////////////////////////////////////

addHandler('POST', '/webhook/.*', async (req, res) => {
  const installation = await authenticate(req);
  if (!installation || typeof installation === 'string') {
    res.writeHead(403);
    res.end(installation || '');
    logRequest(req, res);
    return;
  }
  const webhookPayload = await getRequestBody(req);
  const destination = `data/webhooks/${installation.clientKey}/${req.url.substring('webhook/'.length)}-${new Date().getTime()}.json`;
  await saveFile(destination, webhookPayload);
  res.writeHead(204);
  res.end('');
  logRequest(req, res);
});


/////////////////////////////////////////////
//////////// PAGE HANDLERS //////////////////
/////////////////////////////////////////////

async function generalPageHandler(req, res) {
  const installation = await authenticate(req);
  if (!installation || typeof installation === 'string') {
    res.writeHead(403);
    res.end(installation || '');
    logRequest(req, res);
    return;
  }

  const projectsResourceUrl = '/rest/api/3/project/search';
  const token = jwtForRequest('GET', projectsResourceUrl, installation.key, installation.sharedSecret);
  const [, responseBody] = await httpsGetString(`${installation.baseUrl}${projectsResourceUrl}`, { headers: { authorization: `JWT ${token}` } });
  res.writeHead(200, { 'Content-type': 'text/html' });
  res.end(dataDumpPage('My Jira Projects', responseBody));
  logRequest(req, res);
}

addHandler('GET', '/show-projects', generalPageHandler);

/////////////////////////////////////////////
//////////// FRONT END //////////////////////
/////////////////////////////////////////////

function escape(s) {
  const lookup = {
    '&': '&amp;',
    '"': '&quot;',
    '<': '&lt;',
    '>': '&gt;',
  };
  return s.replace(/[&"<>]/g, (c) => lookup[c]);
}

function dataDumpPage(title, data) {
  try {
    data = JSON.stringify(JSON.parse(data), null, 2);
  } catch {
    // keep the data as a string
  }
  return `<!doctype html>
    <html lang="en">
        <head>
            <link rel="stylesheet" href="https://unpkg.com/@atlaskit/css-reset@2.0.0/dist/bundle.css" media="all">
            <script src="https://connect-cdn.atl-paas.net/all.js" async></script>
            <title>Connect iframe</title>
        </head>
        <body>
                <h1>${escape(title)}</h1>
                <pre>${escape(data)}</pre>
        </body>
    </html>
    `;
}


/////////////////////////////////////////////
///////////////// AUTH //////////////////////
/////////////////////////////////////////////

/**
 * The supplied path must already be properly percent encoded and have any base url path prefix stripped off (but retain a leading slash).
 * NB: Some particular encoding values to be aware of:
 *  A whitespace character must be encoded as "%20"
 *  "," must be encoded as "%2C"
 *  "+" must be encoded as "%2B"
 *  "*" must be encoded as "%2A"
 *  "~" must stay as "~"
 */
function queryStringHash(method, rawPath) {
  const canonicalMethod = method.toUpperCase();
  const [path, ...searches] = rawPath.split('?');
  const search = searches.join('?');
  const canonicalUrl = path.replace(/\/$/, '') || '/';
  const canonicalQueryParams = search
    .split('&')
    .filter((param) => param && param !== 'jwt' && !param.startsWith('jwt='))
    .map((entry) => entry.split('='))
    .sort(([ak, av = ''], [bk, bv = '']) => ak.localeCompare(bk) || av.localeCompare(bv))
    .reduce((acc, current, currentIndex) => {
      const [currentKey, currentValue = ''] = current;
      if (currentIndex === 0) {
        return [[currentKey, [currentValue]]];
      }
      const [[previousKey, previousValues]] = acc.slice(-1);
      if (currentKey !== previousKey) {
        return [...acc, [currentKey, [currentValue]]];
      }
      return [...acc.slice(0, -1), [previousKey, [...previousValues, currentValue]]];
    }, [])
    .map(([key, values]) => `${key}=${values.join(',')}`)
    .join('&');
  const canonicalRequest = `${canonicalMethod}&${canonicalUrl}&${canonicalQueryParams}`;
  return crypto.createHash('sha256').update(canonicalRequest).digest('hex');
}

function base64UrlFromBase64(base64) {
  return base64
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}

function base64FromBase64Url(base64Url) {
  return (base64Url + '==='.slice((base64Url.length + 3) % 4))
    .replace(/-/g, '+')
    .replace(/_/g, '/');
}

function utf8StringFromBase64Url(base64Url) {
  return Buffer.from(base64FromBase64Url(base64Url), 'base64').toString('utf8');
}

function base64Url(string) {
  return Buffer
    .from(string, 'utf-8')
    .toString('base64')
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}

function hmacSha256Sign(payload, secret) {
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(payload);
  return base64UrlFromBase64(hmac.digest('base64'));
}

async function getInstallation(clientKey) {
  try {
    return JSON.parse(await loadFile(`data/installations/${clientKey}.json`));
  } catch (e) {
    return null;
  }
}

function extractJWT(req) {
  const jwtHeader = (req.headers.authorization || '').substring('JWT '.length);
  const queryStringJwt = queryString.parse(req.url.split('?')[1] || '').jwt;
  const jwt = jwtHeader || queryStringJwt || '';
  return jwt.split('.');
}

function validateAndGetClaims(headerSegment, bodySegment, signedSegment, expectedAudience) {
  if (!headerSegment || !bodySegment || !signedSegment) {
    return 'jwt-missing-segments';
  }
  let claims;
  try {
    claims = JSON.parse(utf8StringFromBase64Url(bodySegment));
  } catch (e) {
    return 'could-not-parse-body';
  }
  if (!claims.iss) {
    return 'no-issuer';
  }
  if (!claims.exp || claims.exp <= Date.now() / 1000) {
    return 'jwt-expired';
  }
  if (expectedAudience && claims.aud !== expectedAudience) {
    return 'jwt-wrong-audience';
  }

  return claims;
}

async function authenticate(req) {
  const [headerSegment, bodySegment, signedSegment] = extractJWT(req);

  if (!headerSegment && !bodySegment && !signedSegment) {
    return 'no-jwt';
  }

  if (!headerSegment || !bodySegment || !signedSegment) {
    return 'jwt-missing-segments';
  }

  const claims = validateAndGetClaims(headerSegment, bodySegment, signedSegment);

  if (!claims || typeof claims === 'string') {
    return claims;
  }

  const installation = await (getInstallation(claims.iss));
  if (!installation) {
    return 'installation-not-found';
  }
  if (!verifySignature(headerSegment, bodySegment, signedSegment, installation.sharedSecret)) {
    return 'signature-invalid';
  }
  return installation;
}

async function authenticateInstallHook(req) {
  const [headerSegment, bodySegment, signedSegment] = extractJWT(req);

  if (!headerSegment && !bodySegment && !signedSegment) {
    return 'no-jwt';
  }

  if (!headerSegment || !bodySegment || !signedSegment) {
    return 'jwt-missing-segments';
  }

  if (!appBaseUrl) {
    return 'missing-app-baseurl'
  }

  const claims = validateAndGetClaims(headerSegment, bodySegment, signedSegment, appBaseUrl);

  if (!claims || typeof claims === 'string') {
    return claims;
  }

  if (!await verifyAsymmetricSignature(headerSegment, bodySegment, signedSegment)) {
    return 'install-signature-invalid';
  }
  return claims;
}

function verifySignature(headerSegment, bodySegment, signedSegment, secret) {
  return hmacSha256Sign(`${headerSegment}.${bodySegment}`, secret) === signedSegment;
}

async function verifyAsymmetricSignature(headerSegment, bodySegment, signedSegment) {
  const header = JSON.parse(utf8StringFromBase64Url(headerSegment));
  const publicKey = await getPublicKey(header.kid);
  const signingInput = [headerSegment, bodySegment].join('.');
  return crypto.createVerify('RSA-SHA256').update(signingInput).verify(publicKey, base64FromBase64Url(signedSegment), 'base64');
}

async function getPublicKey(keyId) {
  const CONNECT_KEYS_CDN_URL = process.env.INSTALL_KEYS_CDN || 'https://connect-install-keys.atlassian.com';
  const cdnUrl = `${CONNECT_KEYS_CDN_URL}/${keyId}`;
  const [, publicKey] = await httpsGetString(cdnUrl)
  return publicKey;
}

function jwtForRequest(method, relativePath, key, secret) {
  const now = Math.floor(new Date().getTime() / 1000);
  const payload = {
    iss: key,
    iat: now,
    exp: now + 500,
    qsh: queryStringHash(method, relativePath),
  };
  return signJwt(base64Url('{"typ":"JWT","alg":"HS256"}'), JSON.stringify(payload), secret);
}

function signJwt(headerSegment, payload, secret) {
  const bodySegment = base64Url((typeof payload === 'string') ? payload : JSON.stringify(payload));
  const signedSegment = hmacSha256Sign(`${headerSegment}.${bodySegment}`, secret);
  return `${headerSegment}.${bodySegment}.${signedSegment}`;
}


//////////////////////////////////////////////
////////////// HTTPS CLIENT //////////////////
//////////////////////////////////////////////

function httpsGetString(url, options) {
  return new Promise((resolve, reject) => {
    https.get(url, options || {}, (resp) => {
      let data = '';
      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        resolve([resp, data]);
      });
    }).on('error', (err) => {
      reject(err);
    });
  });
}


/////////////////////////////////////////////
////////////// HTTP SERVER //////////////////
/////////////////////////////////////////////

function logRequest(req, res) {
  console.log(`${new Date().toISOString()} ${req.method} ${req.url} ${res.statusCode}`);
}

function handle(req, res) {
  const pathname = (req.url || '').replace(/\?.*/g, '');
  const methodHandlers = handlers[req.method.toUpperCase()] || {};
  for (const [path, handler] of Object.entries(methodHandlers)) {
    if (new RegExp(`^${path}$`).test(pathname)) {
      return handler(req, res)
        .then(() => logRequest(req, res))
        .catch((e) => {
          console.error(e);
          res.writeHead(500);
          res.end('INTERNAL SERVER ERROR');
          logRequest(req, res);
        });
    }
  }
  res.writeHead(404);
  res.end('NOT FOUND');
  logRequest(req, res);
}

function onServerStarted() {
  console.log(`Server started on port ${port}`);
}

function addHandler(method, path, handler) {
  handlers[method.toUpperCase()][path] = handler;
}

function getRequestBody(req) {
  return new Promise((resolve => {
    let body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      body = Buffer.concat(body).toString();
      resolve(body);
    });
  }));
}


/////////////////////////////////////////////
////////////// FILE SYSTEM //////////////////
/////////////////////////////////////////////

function makeDirectoryForFile(destination) {
  return new Promise((resolve, reject) => {
    fs.mkdir(path.dirname(destination), { recursive: true }, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function loadFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'UTF-8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function saveFile(destination, fileStringContents) {
  return makeDirectoryForFile(destination).then(() =>
    new Promise((resolve, reject) => {
      fs.writeFile(destination, fileStringContents, { encoding: 'UTF-8' }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    }),
  );
}


/////////////////////////////////////////////
/////////////////// INIT ////////////////////
/////////////////////////////////////////////

http.createServer(handle).listen(port, onServerStarted);
